﻿Shader "Unlit/6D-Visualizer"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)

        _Scale ("Scale", float) = 1

        _OffsetScale("Offset Scale", float) = 1

        _OffsetVector("Offset Vector", Vector) = (1,1,1,1)
        _OffsetTScale("Offset T Scale", float) = 1
        _OffsetMScale("Offset M Scale", float) = 1

        _ZScaling("Z Scaling", float) = 0.9

        _Coordinate("Coordinate", Vector) = (0,0,0,0)

        _BoxHeight("Box Height", float) = 1.0
        _BoxWidth("Box Width", float) = 1.0

        _Min0("Min int0", int) = 0
        _Min1("Min int1", int) = 0
        _Min2("Min int2", int) = 0
        _Min3("Min int3", int) = 0
        _Min4("Min int4", int) = 0
        _Min5("Min int5", int) = 0
        _Min6("Min int6", int) = 0

        _Max0("Max int0", int) = 6
        _Max1("Max int1", int) = 6
        _Max2("Max int2", int) = 6
        _Max3("Max int3", int) = 6
        _Max4("Max int4", int) = 6
        _Max5("Max int5", int) = 6
        _Max6("Max int6", int) = 6

        _StartTime("Start Time", int) = 0
        _StartMult("Start Multiverse", int) = 0


    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
        ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
        Blend SrcAlpha OneMinusSrcAlpha

        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;

                int3 xyt : TEXCOORD1;
                int3 zwm : TEXCOORD2;


            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Scale;

            float _OffsetScale;

            float4 _OffsetVector;
            float _OffsetTScale;
            float _OffsetMScale;

            float4 _Color;
            float _ZScaling;

            float4 _Coordinate;

            float _BoxHeight;
            float _BoxWidth;


            int _Min0;
            int _Min1;
            int _Min2;
            int _Min3;
            int _Min4;
            int _Min5;
            int _Min6;

            int _Max0;
            int _Max1;
            int _Max2;
            int _Max3;
            int _Max4;
            int _Max5;
            int _Max6;

            int _StartTime;
            int _StartMult;


            float isValidTimelineCoord(int t, int m, int m_rel)
            {
                int t0 = t;
                int m0 = m_rel + 3;

                bool is0 = (m0 == 0) && (_Min0 <= t0) && (t0 <= _Max0) && ((t0 > 0) || m == 0);
                bool is1 = (m0 == 1) && (_Min1 <= t0) && (t0 <= _Max1) && ((t0 > 0) || m == 0);
                bool is2 = (m0 == 2) && (_Min2 <= t0) && (t0 <= _Max2) && ((t0 > 0) || m == 0);
                bool is3 = (m0 == 3) && (_Min3 <= t0) && (t0 <= _Max3) && ((t0 > 0) || m == 0);
                bool is4 = (m0 == 4) && (_Min4 <= t0) && (t0 <= _Max4) && ((t0 > 0) || m == 0);
                bool is5 = (m0 == 5) && (_Min5 <= t0) && (t0 <= _Max5) && ((t0 > 0) || m == 0);
                bool is6 = (m0 == 6) && (_Min6 <= t0) && (t0 <= _Max6) && ((t0 > 0) || m == 0);

                return is0 || is1 || is2 || is3 || is4 || is5 || is6;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.normal = v.normal;
                
                float visible = 1.0;

                float3 rt = float3(1, 0, 0)*_OffsetScale;
                float3 up = float3(0, 1, 0)*_OffsetScale;
                float3 fw = float3(0, 0, 1)*_OffsetScale;

                float x_offset = _OffsetVector.x;
                float y_offset = _OffsetVector.y;
                float z_offset = _OffsetVector.z;
                float w_offset = _OffsetVector.w;
                float t_offset = _OffsetTScale;
                float m_offset = _OffsetMScale;


                float3 xvec = x_offset * rt;
                float3 zvec = z_offset * up;
                
                float3 yvec = y_offset * fw;
                float3 wvec = w_offset * fw;

                
                float3 tvec = t_offset * rt;
                float3 mvec = m_offset * fw;

                int co_x = (_Coordinate.x + 0.5) + ((_Coordinate.x + 0.5) < 0);
                int co_y = (_Coordinate.y + 0.5) + ((_Coordinate.y + 0.5) < 0);
                int co_z = (_Coordinate.z + 0.5) + ((_Coordinate.z + 0.5) < 0);
                int co_w = (_Coordinate.w + 0.5) + ((_Coordinate.w + 0.5) < 0);

                

                

                int offset = 3 + 3 * 8;
                float3 vertex_pos = v.vertex*_Scale*float3(-1.0, 1.0, 1.0) + float3(1.0, 1.0, 1.0) * offset;
                
                float3 fractional_vertex = (frac(v.vertex*_Scale) - float3(0.5,0.0,0.5))*float3(_BoxWidth, _BoxHeight, _BoxWidth);

                int x_in = vertex_pos.x;
                int y_in = vertex_pos.y;
                int z_in = vertex_pos.z;

                int x_out = (x_in & 7) - 3;
                int y_out = (y_in & 7) - 3;
                int t_out = (z_in & 7) - 3;

                int z_out = ((x_in >> 3) & 7) - 3;
                int w_out = ((y_in >> 3) & 7) - 3;
                int m_out = ((z_in >> 3) & 7) - 3;
                
                int x_valid = (-1 < x_out + co_x) && (x_out + co_x < 4);
                int y_valid = (-1 < y_out + co_y) && (y_out + co_y < 4);
                int z_valid = (-1 < z_out + co_z) && (z_out + co_z < 4);
                int w_valid = (-1 < w_out + co_w) && (w_out + co_w < 4);

                float xy_scaler = pow(_ZScaling, z_out + co_z);
                float xy_scaler0 = pow(_ZScaling, co_z);

                int m_abs = _StartMult + m_out;
                int t_abs = _StartTime + t_out*2;

                float valid_time = isValidTimelineCoord(t_abs, m_abs, m_out);

                visible = visible * x_valid * y_valid * z_valid * w_valid *valid_time;


                //float3 total_offset = xvec * x_out
                //                    + yvec * y_out
                //                    + zvec * z_out
                //                    + wvec * w_out
                //                    + tvec * t_out
                //                    + mvec * m_out;

                float3 total_offset = xvec * (x_out - 1.5 + co_x) * xy_scaler + (1.5 - co_x) * xvec * xy_scaler0
                                    + yvec * (y_out - 1.5 + co_y) * xy_scaler + (1.5 - co_y) * yvec * xy_scaler0
                                    + zvec * z_out
                                    + wvec * w_out
                                    + tvec * t_out
                                    + mvec * m_out;

                o.xyt = int3(x_out, y_out, z_out);
                o.zwm = int3(z_out, w_out, m_out);

                
                o.vertex = UnityObjectToClipPos(fractional_vertex*visible + total_offset);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 xyt = float4(i.xyt, 4.0) * 0.25;
                float4 zwm = float4(i.zwm, 4.0) * 0.25;


                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * (1.0 + dot(i.normal, _WorldSpaceLightPos0))*0.5;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col*_Color;
            }
            ENDCG
        }
    }
}
