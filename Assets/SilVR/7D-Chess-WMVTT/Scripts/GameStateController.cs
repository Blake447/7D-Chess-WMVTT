﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;

public class GameStateController : UdonSharpBehaviour
{
    [UdonSynced]
    public int[] move_history;
    public int[] move_history_buffer;

    [UdonSynced]
    public bool isWhitesGlobalTurn = true;

    [UdonSynced]
    string PlayerOne = "";
    
    [UdonSynced]
    string PlayerTwo = "";

    [UdonSynced]
    int roll_back_turn = 0;

    public PieceInterface white_interface;
    public PieceInterface black_interface;


    public BoardSerializer root_serializer;
    public BoardSerializer stack_top;
    public BoardSerializer front_root;


    public GameObject rootObject;
    public GameObject cursor_object;
    public GameObject present_root;

    public int present_tcoord;

    public BoardSerializer a;
    public BoardSerializer b;

    int stack_size;
    int turns = 0;
    int instantiations = 1;

    BoardSerializer turn_stack_top;
    BoardSerializer board_buffer;

    int square_buffer;
    bool buffered;

    BoardSerializer preview_buffer0;
    BoardSerializer preview_buffer1;

    private float xy_offset = 0.0825f;
    private float z_offset = 0.1235f;
    private float z_scaling = 0.9f;
    private float w_offset = 0.4125f;
    private float t_offset = 1.0f;
    private float m_offset = 2.5f;

    Vector3 root;
    Vector3 rt = new Vector3(1, 0, 0);
    Vector3 up = new Vector3(0, 1, 0);
    Vector3 fw = new Vector3(0, 0, 1);

    int[] offsets_sorted = new int[6];
    int[] offsets = new int[6];

    public GameObject VisualizerRoot;
    public Material VisualizerMaterial;
    public Material VisualizerMaterial0;

    public GameObject[] Visualizers;

    public bool requiredMoveIsWhite;

    public bool bypassGlobalTurn;
    public bool bypassInterface;

    int NO_BOARD_FOUND = 404;

    private MaterialPropertyBlock white_property_block;
    private MaterialPropertyBlock black_property_block;


    public Text player_one_text;
    public Text player_two_text;


    public GameObject whites_turn_indicator;
    public GameObject blacks_turn_indicator;

    public GameObject whites_turn_indicator0;
    public GameObject blacks_turn_indicator0;

    public int[] front_coords;
    public int[] root_coords;

    public bool isDeserializing;
    public int deserializingStep = 0;

    private Vector3 white_start;
    private Vector3 black_start;

    public GameObject CameraTarget;

    public GameObject floor_collider;
    
    public float max_height;
    public float min_height;
    public float reset_height;
    public float current_height;
    public float increment_height;

    public InputField inputField;

    int ENCODER_LENGTH = 5;

    ///////////////////////////////////////////////
    ///                                         ///
    ///       Startup and VRC Functions         ///
    ///                                         ///
    ///////////////////////////////////////////////

    void Start()
    {

        white_property_block = new MaterialPropertyBlock();
        black_property_block = new MaterialPropertyBlock();

        white_property_block.SetFloat("_Parity", 0.0f);
        black_property_block.SetFloat("_Parity", 1.0f);

        UpdateBasis();

        root_serializer.InitializeBoard();
        stack_top.InitializeBoard();

        root_serializer.SerializeBoard();
        root_serializer.ReceivePayload(root_serializer);

        white_interface.SetRootObject(rootObject);
        white_interface.SetGameStateController(this);

        black_interface.SetRootObject(rootObject);
        black_interface.SetGameStateController(this);

        white_start = white_interface.transform.position;
        black_start = black_interface.transform.position;

        turn_stack_top = root_serializer;

        for (int i = 0; i < 4; i++)
        {
            InstantiateToStack();
        }
    }

    public void Update()
    {
        if (isDeserializing)
        {
            AdvanceDeserialization();
        }
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///        External Object Control          ///
    ///                                         ///
    ///////////////////////////////////////////////

    public void RaiseFloor()
    {
        current_height += increment_height;
        Mathf.Clamp(current_height, min_height, max_height);
        if (floor_collider != null) { floor_collider.transform.position = Vector3.zero + Vector3.up * current_height; }
    }
    public void LowerFloor()
    {
        current_height -= increment_height;
        Mathf.Clamp(current_height, min_height, max_height);
        if (floor_collider != null) { floor_collider.transform.position = Vector3.zero + Vector3.up * current_height; }
    }
    public void ResetFloor()
    {
        current_height = reset_height;
        if (floor_collider != null) { floor_collider.transform.position = Vector3.zero + Vector3.up * current_height; }
    }

    public void RecallWhite()
    {
        if (isPlayerWhite())
        {
            white_interface.gameObject.transform.position = white_start;
        }
    }
    public void RecallBlack()
    {
        if (isPlayerBlack())
        {
            black_interface.gameObject.transform.position = black_start;
        }
    }

    public string GetLocalNameSafe()
    {
        string name = "";
        if (Networking.LocalPlayer != null) { name = Networking.LocalPlayer.displayName; }
        return name;
    }


    public bool isPlayerInGame()
    {
        string local_name = GetLocalNameSafe();
        return (local_name == PlayerOne) || (local_name == PlayerTwo);
    }
    public bool isPlayerWhite()
    {
        string local_name = GetLocalNameSafe();
        return (local_name == PlayerOne);
    }
    public bool isPlayerBlack()
    {
        string local_name = GetLocalNameSafe();
        return (local_name == PlayerTwo);
    }

    public MaterialPropertyBlock ReturnPropertyBlock(int isWhite)
    {
        if (isWhite == 1) { return white_property_block; }
        return black_property_block;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///     Network And Game State Control      ///
    ///                                         ///
    ///////////////////////////////////////////////

    public void LoadGame()
    {
        if (Networking.IsMaster)
        {
            string encodedGame = inputField.text;
            int length = encodedGame.Length;

            int doub = ENCODER_LENGTH * 2;
            int mone = ENCODER_LENGTH - 1;

            move_history = new int[length / ENCODER_LENGTH];

            for (int i = 0; i < length / doub; i++)
            {
                if (i * doub + mone < length)
                {
                    string from = "";
                    string to = "";
                    for (int j = 0; j < ENCODER_LENGTH; j++)
                    {
                        from += encodedGame[doub * i + j];
                        to += encodedGame[doub * i + ENCODER_LENGTH +  j];

                    }

                    move_history[i * 2 + 0] = DecodeMoveToInt(from);
                    move_history[i * 2 + 1] = DecodeMoveToInt(to);
                }
            }

            HardSync();
            UpdateGameState();
            Networking.SetOwner(Networking.LocalPlayer, this.gameObject);
            RequestSerialization();
        }

    }

    public int DecodeMoveToInt(string move)
    {
        int a = 0;
        for (int i = 0; i < ENCODER_LENGTH; i++)
        {
            a += (move[i] - '0') << (6 * i);
        }
        return a;
    }

    public string EncodeMoveToString(int from, int to)
    {
        string a = "";
        string b = "";

        for (int i = 0; i < ENCODER_LENGTH; i++)
        {
            int f = (from >> (i * 6) ) & 63;
            int t = (to   >> (i * 6) ) & 63;
            a += (char)('0' + f);
            b += (char)('0' + t);
        }
        return a + b;
    }

    public void PrintHistoryToLog()
    {
        string encoded_history = "";


        for (int i = 0; i < move_history.Length / 2; i++)
        {
            encoded_history = encoded_history + EncodeMoveToString(move_history[2 * i], move_history[2 * i + 1]);
        }
        Debug.Log(encoded_history);
        inputField.text = encoded_history;
    }

    public void HardSync()
    {
        move_history_buffer = move_history;
        int agreement = 0;
        RevertToLocal(agreement);
        deserializingStep = 0;
        isDeserializing = true;
    }
    
    public void SimulateFullDeserialization()
    {
        StartDeserialization();
    }

    public void StartDeserialization()
    {
        int agreement = DetermineAgreement();
        int latest_turn = turns;
        int target = Mathf.Min(latest_turn, agreement);
        RevertToLocal(target);

        deserializingStep = target;
        isDeserializing = true;
    }
    public void AdvanceDeserialization()
    {
        if ( (deserializingStep*2 + 1) < move_history.Length)
        {
            ApplyMoveFromHistory(move_history[deserializingStep * 2], move_history[deserializingStep * 2 + 1]);
            deserializingStep++;
        }
        else
        {
            EndDeserialization();
        }
    }
    public void EndDeserialization()
    {
        isDeserializing = false;
        UpdateGameState();
        if (turns == move_history.Length / 2)
        {
            move_history_buffer = move_history;
        }
    }

    public void ApplyFromDisagreementLocal()
    {
        // Determine when the history buffer and the networked history 
        int agreement = DetermineAgreement();

        // Revert to the point the history buffer and networked history agree
        RevertToLocal(agreement);

        // Starting at the point our histories agree, and going up to half the length of the array
        int j = agreement;
        for (int i = j; i < move_history.Length / 2; i++)
        {
            // apply integers in pairs that encode the previous moves of the game board
            ApplyMoveFromHistory(move_history[2 * i], move_history[2 * i + 1]);
        }

        // Update the present coordinate
        present_tcoord = DeterminePresent();

        // Indicate all the boards neccesary
        IndicateRequiredBoards();

        // And have all the boards display their pointers
        //DisplayAllPointers();
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///             Game Controls               ///
    ///                                         ///
    ///////////////////////////////////////////////


    public void SubmitTurn()
    {
        if (isPlayerInGame())
        {
            bool player_white = PlayerOne == GetLocalNameSafe();
            bool player_black = PlayerTwo == GetLocalNameSafe();

            bool is_correct_player = (player_white == isWhitesGlobalTurn) || (player_black != isWhitesGlobalTurn);

            if (is_correct_player && (isWhitesGlobalTurn != ( (present_tcoord & 1) == 0 )) )
            {
                roll_back_turn = turns;
                isWhitesGlobalTurn = (present_tcoord & 1) == 0;
                whites_turn_indicator.SetActive(isWhitesGlobalTurn);
                blacks_turn_indicator.SetActive(!isWhitesGlobalTurn);

                whites_turn_indicator0.SetActive(isWhitesGlobalTurn);
                blacks_turn_indicator0.SetActive(!isWhitesGlobalTurn);

                UpdateGameState();
                Networking.SetOwner(Networking.LocalPlayer, this.gameObject);
                RequestSerialization();
            }

        }
    }
    public void ClearTurn()
    {
        if (isPlayerBlack() && !isWhitesGlobalTurn)
        {
            while (turn_stack_top.TurnStackNext != null && (turn_stack_top.tcoord & 1) == 0)
            {
                RevertToNetworked(turns - 1);
            }
            UpdateGameState();
        }
        if (isPlayerWhite() && isWhitesGlobalTurn)
        {
            while (turn_stack_top.TurnStackNext != null && (turn_stack_top.tcoord & 1) == 1)
            {
                RevertToNetworked(turns - 1);
            }
            UpdateGameState();
        }
    }

    public void JoinPlayerOne()
    {

        if (Networking.LocalPlayer != null)
        {
            VRCPlayerApi p1 = Networking.LocalPlayer;
            string name = p1.displayName;
            PlayerOne = name;

            UpdatePlayerNames();
            Networking.SetOwner(Networking.LocalPlayer, this.gameObject);
            RequestSerialization();
        }
    }
    public void JoinPlayerTwo()
    {
        if (Networking.LocalPlayer != null)
        {
            VRCPlayerApi p2 = Networking.LocalPlayer;
            string name = p2.displayName;
            PlayerTwo = name;

            UpdatePlayerNames();
            Networking.SetOwner(Networking.LocalPlayer, this.gameObject);
            RequestSerialization();
        }
    }
    public void UpdatePlayerNames()
    {
        player_one_text.text = PlayerOne;
        player_two_text.text = PlayerTwo;
    }

    public void ClearPlayers()
    {
        PlayerOne = "";
        PlayerTwo = "";
        UpdatePlayerNames();
    }

    public void ResetBoard()
    {
        if (Networking.IsMaster)
        {
            ClearPlayers();
            isWhitesGlobalTurn = true;
            RevertToNetworked(0);
        }
    }
    public void UndoMove()
    {
        if (isPlayerInGame())
        {
            RevertToNetworked(turns - 1);
            UpdateGameState();
        }
    }

    public void UpdateGameState()
    {
        DeterminePresent();
        UpdatePlayerNames();
        IndicateRequiredBoards();

        whites_turn_indicator.SetActive(isWhitesGlobalTurn);
        blacks_turn_indicator.SetActive(!isWhitesGlobalTurn);

        whites_turn_indicator0.SetActive(isWhitesGlobalTurn);
        blacks_turn_indicator0.SetActive(!isWhitesGlobalTurn);
    }

    public void InitiateDebugSequence()
    {
        int[] debug_history = new int[12];

        debug_history[0 ] = 203;
        debug_history[1 ] = 199;
        debug_history[2 ] = 263;
        debug_history[3 ] = 267;
        debug_history[4 ] = 766;
        debug_history[5 ] = 246;
        debug_history[6 ] = 131377;
        debug_history[7 ] = 313;
        debug_history[8 ] = 131835;
        debug_history[9 ] = 131831;
        debug_history[10] = 131890;
        debug_history[11] = 826;

        move_history = debug_history;
        ApplyFromDisagreementLocal();

    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///          Networking Functions           ///
    ///                                         ///
    ///////////////////////////////////////////////

    // May God help you if you are looking to explore this section.

    public override void OnDeserialization()
    {
        StartDeserialization();
    }

    public int DetermineAgreement()
    {
        // Get the length of the shortest history buffer
        int length = Mathf.Min(move_history_buffer.Length, move_history.Length);

        // If any of them have any length whatsoever
        if (length > 0)
        {
            // Start at the last element of the shortest array
            int i = length - 1;

            // While the arrays have different elements in them and our index is not zero, decrement our index
            while ((move_history_buffer[i] != move_history[i]) && (i > 0)) { i = i - 1; }

            // Return the turn that this length of an array corresponds to
            return (i / 2);
        }
        return 0;
    }

    public void RevertToLocal(int n)
    {
        // Initialize two sentinals to the top of the stack recording what boards have been placed down
        BoardSerializer reversion_sentinal0 = turn_stack_top;
        BoardSerializer reversion_sentinal1 = turn_stack_top;
        
        // If for whatever reason our turn target is negative, set it to zero
        if (n < 0) { n = 0; }

        // While we have not reached the desired turn number, and while we still have boards to progress to afterwards
        while (reversion_sentinal0.turn_number > n && reversion_sentinal0.TurnStackNext != null)
        {
            // Advance the first sentinal back once
            reversion_sentinal0 = reversion_sentinal0.TurnStackNext;

            // Place the second sentinal back on the stack
            PlaceOnPreviewStack(reversion_sentinal1);

            // Then move the second sentinal to the first one to catch up
            reversion_sentinal1 = reversion_sentinal0;
        }

        // Update the turn count to whatever we reached
        turns = reversion_sentinal0.turn_number;
    }

    void ApplyMoveFromHistory(int i, int j)
    {
        // Deserialize the move information from the first integer
        int id_from = i & 255;
        int t_from = (i >> 8) & 255;
        int m_from = i_to_m((i >> 16) & 255);

        // Deserialize the move information from the second integer
        int id_to = j & 255;
        int t_to = (j >> 8) & 255;
        int m_to = i_to_m((j >> 16) & 255);

        // Get the boards we are attempting to move pieces to and from
        a = GetBoardSerializer(t_from, m_from);
        b = GetBoardSerializer(t_to, m_to);

        // Do not write history when applying a move from history
        bool write_history = false;

        // And move the piece as neccesary.
        if (a != null && b != null) { MovePiece(a, id_from, b, id_to, write_history); }
    }

    public void WriteHistory(BoardSerializer ser_from, int id_from, BoardSerializer ser_to, int id_to)
    {
        // Create a new array two longer than the current networked history
        move_history_buffer = new int[move_history.Length + 2];

        // Copy the networked history into the history buffer
        for (int i = 0; i < move_history.Length; i++) { move_history_buffer[i] = move_history[i]; }
        
        // Serialize the to and from coordinates for the move we are enacting
        move_history_buffer[move_history.Length] = SerializeMove(ser_from, id_from);
        move_history_buffer[move_history.Length + 1] = SerializeMove(ser_to, id_to);

        // assign the move history buffer to the networked history
        move_history = move_history_buffer;

        // Set the GameStateControllers owner to be the local player
        Networking.SetOwner(Networking.LocalPlayer, this.gameObject);

        // Request Serialization.
        RequestSerialization();

    }

    public void ApplyFromDisagreement()
    {
        int agreement = DetermineAgreement();
        RevertTo(agreement);
        int j = 2 * agreement;
        if ((move_history.Length & 1) == 0)
        {
            for (int i = j; i < move_history.Length / 2; i++)
            {
                ApplyMoveFromHistory(move_history[2 * i], move_history[2 * i + 1]);
            }
        }
    }

    public void RevertHistory(int turn)
    {
        int target_length = Mathf.Min(turn * 2, move_history.Length);
        
        move_history_buffer = new int[target_length];
        for (int i = 0; i < target_length; i++) { move_history_buffer[i] = move_history[i]; }
        move_history = new int[move_history_buffer.Length];
        for (int i = 0; i < move_history_buffer.Length; i++) { move_history[i] = move_history_buffer[i]; }

    }

    public void RevertToNetworked(int n)
    {
        // Initialize two sentinals to the top of the stack recording what boards have been placed down
        BoardSerializer reversion_sentinal0 = turn_stack_top;
        BoardSerializer reversion_sentinal1 = turn_stack_top;

        // If for whatever reason our turn target is negative, set it to zero
        if (n < 0) { n = 0; }

        // While we have not reached the desired turn number, and while we still have boards to progress to afterwards
        while (reversion_sentinal0.turn_number > n && reversion_sentinal0.TurnStackNext != null)
        {
            // Advance the first sentinal back once
            reversion_sentinal0 = reversion_sentinal0.TurnStackNext;

            // Place the second sentinal back on the stack
            PlaceOnPreviewStack(reversion_sentinal1);

            // Then move the second sentinal to the first one to catch up
            reversion_sentinal1 = reversion_sentinal0;
        }

        // Update the turn count to whatever we reached
        turns = reversion_sentinal0.turn_number;

        // Determine the length the history buffer should be. Consider reverting to the second turn thats been made. We should then have 4
        // integers in our turn buffer. So reverting to the second (zero indexed) board should take a length of 2n. Alternatively,
        // make sure that we dont write outside of our bounds if say, a large number was passed in.
        int length = Mathf.Min(2 * n, move_history.Length);

        // Initialize a new history buffer of our target length
        move_history_buffer = new int[length];

        // Copy the contents of move_history into the buffer. Note that since we min() with move_history length we will never step out of bounds.
        for (int i = 0; i < move_history_buffer.Length; i++) { move_history_buffer[i] = move_history[i]; }

        // Assign the buffer to the networked history
        move_history = move_history_buffer;

        UpdateGameState();

        // Set the GameStateControllers owner to be the local player
        Networking.SetOwner(Networking.LocalPlayer, this.gameObject);

        // Request Serialization.
        RequestSerialization();


    }

    public void RevertTo(int n)
    {
        BoardSerializer reversion_sentinal0 = turn_stack_top;
        BoardSerializer reversion_sentinal1 = turn_stack_top;
        if (n < 0) { n = 0; }
        while (reversion_sentinal0.turn_number > n && reversion_sentinal0.TurnStackNext != null)
        {
            reversion_sentinal0 = reversion_sentinal0.TurnStackNext;
            PlaceOnPreviewStack(reversion_sentinal1);
            reversion_sentinal1 = reversion_sentinal0;
        }

        turns = reversion_sentinal0.turn_number;
    }

    // Encode a coordinate into a single integer for the history ledger.
    int SerializeMove(BoardSerializer b, int id)
    {
        int serialized = ((m_to_i(b.mcoord) & 255) << 16) + ((b.tcoord & 255) << 8) + id;
        return serialized;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///         Execute Buffer control          ///
    ///                                         ///
    ///////////////////////////////////////////////

    // Main buffer control. When the interfaces are interacted with, they will attempt to snap to the nearest board coordinate.
    // They will then send the x,y,z,w,t and i coordinates to the Game State Controller, as well as whether or not the snap
    // succeeded, and if the interface is white's or black's.

    public void ExecuteBufferControl(int xi, int yi, int zi, int wi, int ti, int mi, bool snapped, bool isWhitesInterface)
    {
        bool matchesGlobalTurn = (isWhitesInterface == isWhitesGlobalTurn);
        bool is_player_in_game = isPlayerInGame();
        
        bool is_player_white = isPlayerWhite();
        bool is_player_black = isPlayerBlack();

        bool isValidPlayer = (is_player_white && isWhitesInterface) || (is_player_black && !isWhitesInterface);

        BoardSerializer bser_found = null;
        if (snapped) { bser_found = GetBoardSerializer(ti, mi); }

        bool set_visualizer = false;
        if (!isDeserializing && bser_found != null)
        {
            if (!buffered)
            {
                // Store the snapped position into our buffer
                square_buffer = CoordToIndex(xi, yi, zi, wi);
                int piece_found = bser_found.GetSquare(square_buffer);
                bool was_piece_found = piece_found != 0;
                if (was_piece_found)
                {
                    SetVisualizers(piece_found, xi, yi, zi, wi, ti, mi);
                    set_visualizer = true;
                }
                else
                {
                    VisualizerRoot.gameObject.SetActive(false);
                }

            }
        }

        // If the interface matches the global turn, and we find a board serializer
        if (  !isDeserializing && ((is_player_in_game && isValidPlayer && matchesGlobalTurn) || bypassGlobalTurn) && bser_found != null)
        {
            // If nothing is in our buffers
            if (!buffered)
            {
                // Store the snapped position into our buffer
                square_buffer = CoordToIndex(xi, yi, zi, wi);
                int piece_found = bser_found.GetSquare(square_buffer);


                // Check if we found a piece and that the piece matches the interface used
                bool was_piece_found = piece_found != 0;
                bool piece_matches_interface = isBlackPiece(piece_found) == !isWhitesInterface;

                // if we found a piece and the piece matches the color interface we used
                if ( was_piece_found && (piece_matches_interface || bypassInterface) )
                {
                    // Store the board we found into the board buffer. Note we already have checked that this is not null
                    board_buffer = bser_found;

                    // Move the cursor to the position specified
                    cursor_object.transform.position = PosFromCoord(xi, yi, zi, wi, ti, mi);

                    // Set the visualizer accordingly
                    SetVisualizers(piece_found, xi, yi, zi, wi, ti, mi);

                    // State that we have something in our buffer
                    buffered = true;
                }
            }
            // If we were already storing stuff in our buffer
            else
            {
                // State we are writing to the history buffer
                bool write_history = true;

                // Move the piece from the buffer to the board we found at the coordinate it snapped to
                MovePiece(board_buffer, square_buffer, bser_found, CoordToIndex(xi, yi, zi, wi), write_history);
                
                // Update all pointers.
                // TODO: Optimize to only display pointers we changed, or even completely remove this as its only a debug function
                //DisplayAllPointers();

                // Hide our cursor object
                cursor_object.transform.position = PosFromCoord(xi, yi, zi, wi, ti, mi) - Vector3.up * 1000;
                
                // Disable the visualizer
                VisualizerRoot.gameObject.SetActive(false);
                
                // Set the buffer to false.
                buffered = false;
            }
        }
        // If the board serializer was null (happens when interface fails a snap), or it was not the players turn
        else
        {
            // Hide our cursor object
            cursor_object.transform.position = PosFromCoord(xi, yi, zi, wi, ti, mi) - Vector3.up * 1000;
            
            if (!set_visualizer)
            {
                // Disable the visualizer
                VisualizerRoot.gameObject.SetActive(false);
            }

            // Clear the buffer, in case it was buffered.
            buffered = false;
        }

        // Determine the present time after an interact
        present_tcoord = DeterminePresent();

        // Indicate which boards are required to be played
        IndicateRequiredBoards();
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///         Piece Movement Control          ///
    ///                                         ///
    ///////////////////////////////////////////////

    // Attempts to move a piece from one board to another, as well as potentially writes to the networked history.
    // Basically runs a bunch of logic to determine what kind of move we made, and if it was valid. It does use a
    // helper function for determining rules for various pieces, but MovePiece() itself is responsible for
    // turn, multiverse, and target occupancy. Movement combinations, occlusion, and pawn attacking are handled by
    // TryMoveRequest(). Also places boards with updated piece states, though it does not handle the pointers itself,
    // and delegates that to PlacePreviewBoards().

    // Returns true if the piece was sucessfully moved
    bool MovePiece(BoardSerializer ser_from, int id_from, BoardSerializer ser_to, int id_to, bool write_history)
    {
        // store the id of the piece we found
        int piece = ser_from.GetSquare(id_from);
        
        // Calculate a couple booleans for various situations
        bool capturing = (ser_to.GetSquare(id_to) != 0) && (isBlackPiece(ser_from.GetSquare(id_from)) != isBlackPiece(ser_to.GetSquare(id_to)));
        
        bool is_players_turn = (((ser_from.tcoord & 1) == 1) == isBlackPiece(piece)) && (((ser_to.tcoord & 1) == 1) == isBlackPiece(piece));
        bool from_playable_board = (ser_from.nt == null);
        bool target_placable = (ser_to.GetSquare(id_to) == 0) || capturing;
        bool from_not_empty = (ser_from.GetSquare(id_from) != 0);
        
        // Combine the above to determine if the piece can be moved
        bool movable = (is_players_turn && from_playable_board && target_placable && from_not_empty);
        bool valid_move = target_placable && from_not_empty && TryMoveRequest(id_from, id_to, ser_from, ser_to, piece, capturing);

        // Dermine what kind of move it was. If it splits off into a new parallel universe, 
        bool is_split_multiverse = ser_to.nt != null;

        // Or if its between two active multiverses
        bool is_multiversal = (ser_to.mcoord != ser_from.mcoord) && from_playable_board && !is_split_multiverse;

        // If the move was valid and we were able to move the piece
        if (valid_move && movable)
        {

            // TODO: Refactor this. We can move the pieces in PlacePreviewBoards(), and we can use the preview buffer info to set
            // the turns.

            // Place our preview boards accordingly
            PlacePreviewBoards(ser_from, ser_to, id_from, id_to, piece);

            Vector3 vertical = Vector3.up * z_offset * 0.25f;
            Vector3 from = PosFromIndex(id_from, ser_from.tcoord, ser_from.mcoord);
            Vector3 to = PosFromIndex(id_to, ser_to.tcoord, ser_to.mcoord);
            ser_from.PointArrowVector(ser_from.GetPieceArrow(), from + vertical, to + vertical, xy_offset);

            // So the PlacePreviewBoards() calls PlaceBoardsAtCoord() which either stores a reference to the board in preview_buffer0
            // if it is the board the piece is moving from, or preview_buffer1 if it is the board it is moving to. Additionally, writing
            // to preview_buffer0 also writes to preview_buffer1 in case the piece is simply moved to the same board it came from.
            if (preview_buffer0 != null) { preview_buffer0.AddPieceID(id_from, 0); }
            if (preview_buffer1 != null)
            {
                preview_buffer1.AddPieceID(id_to, piece);
                if (piece == 6)
                {
                    if ( ((id_to >> 6) & 3) == 3 && ((id_to >> 2) & 3) == 3) { preview_buffer1.AddPieceID(id_to, 2); }
                }
                if (piece == 12)
                {
                    if (((id_to >> 6) & 3) == 0 && ((id_to >> 2) & 3) == 0) { preview_buffer1.AddPieceID(id_to, 8); }
                }
            }


            // Add one to the total turn count
            turns++;

            // If we either split off a new multiverse or we traveled between two active, we spawned two boards
            if (is_split_multiverse || is_multiversal)
            {
                // On both of those boards, record the current turn count
                turn_stack_top.turn_number = turns;
                turn_stack_top.TurnStackNext.turn_number = turns;

            }
            else { turn_stack_top.turn_number = turns; }

            // If we are writing history (i.e., not calling this from OnDeserialization() )
            if (write_history)
            {
                // Record the move we made
                WriteHistory(ser_from, id_from, ser_to, id_to);
            }

        }
        return valid_move && movable;
    }

    // Just a quick sorting algorithm that works well for small arrays
    void SortOffsetsSorted()
    {
        int buff = 0;
        for (int i = 1; i < 6; i++)
        {
            int j = i;
            while (j > 0 && offsets_sorted[j - 1] > offsets_sorted[j])
            {
                buff = offsets_sorted[j];
                offsets_sorted[j] = offsets_sorted[j - 1];
                offsets_sorted[j - 1] = buff;
                j--;
            }
        }
    }

    // Returns true for valid piece moves
    bool TryMoveRequest(int id_start, int id_end, BoardSerializer bfrom, BoardSerializer bto, int piece_id, bool capturing)
    {
        bool false_state = false;

        // Divide by two, discarding remainder. Converts from turns to t increments.
        int t_end = bto.tcoord / 2;
        int t_start = bfrom.tcoord / 2;

        // Calculate the t and m offsets.
        int t_off = t_end - t_start;
        int m_off = bto.mcoord - bfrom.mcoord;

        // extract the coordinates from the indexes given find the offsets between them.
        int x = ((id_start >> 0 * 2) & 3) - ((id_end >> 0 * 2) & 3);
        int y = ((id_start >> 1 * 2) & 3) - ((id_end >> 1 * 2) & 3);
        int z = ((id_start >> 2 * 2) & 3) - ((id_end >> 2 * 2) & 3);
        int w = ((id_start >> 3 * 2) & 3) - ((id_end >> 3 * 2) & 3);

        // Store the offsets into an array of length 6. Warning: This is a global variable, and will remain as such
        // to prevent reallocating memory every time a move request is fired off. Same with offsets_sorted.
        offsets[0] = x;
        offsets[1] = y;
        offsets[2] = z;
        offsets[3] = w;
        offsets[4] = t_off;
        offsets[5] = m_off;

        // Copy the absolute value of the offsets into another array of length 6.
        for (int i = 0; i < 6; i++) { offsets_sorted[i] = Mathf.Abs(offsets[i]); }
        
        // Sort the array offsets_sorted
        SortOffsetsSorted();

        // If the piece moves in two forward directions or two lateral directions at the same time, it violates the rules of SilVR 4D Chess, and
        // cannot be a valid move in this game either.
        if ((x != 0 && z != 0) || (y != 0 && w != 0)) { return false_state; }

        // Assume correct motion, so that we can zero it out if the test remains invalid. Note that undefined pieces always return valid moves
        bool valid_motion = true;
        
        // Declare a flag that tells us if we need to check that all squares up to the target are empty before moving. Used for rooks, bishops, and queens.
        bool occlusionCheck = false;

        // If the king is moving, flag the occlusion check then
        if (piece_id == 1 || piece_id == 7)
        {    
            // Check that the king moved in 1 or 0 squares in all directions. Note that the lateral-forward rule excludes pentagonals and hexagonals.
            for (int i = 0; i < 6; i++) { valid_motion = valid_motion && (offsets_sorted[i] == 1 || offsets_sorted[i] == 0); }
        }
        // If the queen is moving
        if (piece_id == 2 || piece_id == 8)
        {    
            occlusionCheck = true;

            // Check that all offsets are equal to the largest, or equal to zero.
            for (int i = 0; i < 6; i++) { valid_motion = valid_motion && (offsets_sorted[i] == offsets_sorted[5] || offsets_sorted[i] == 0); }
        }
        // If the bishop is moving, flag the occlusion check then
        if (piece_id == 3 || piece_id == 9)
        {
            occlusionCheck = true;

            // Check to see the bishop is moving along only two directions, then that the two directions that are moving are equal to one another
            for (int i = 0; i < 4; i++) { valid_motion = valid_motion && (offsets_sorted[i] == 0); }
            valid_motion = valid_motion && (offsets_sorted[4] == offsets_sorted[5]);
        }
        // If the knight is moving
        if (piece_id == 4 || piece_id == 10)
        {
            // Check to see it is moving along only two directions, then that the second largest direction of travel is one, and the largest is 2.
            for (int i = 0; i < 4; i++)  { valid_motion = valid_motion && (offsets_sorted[i] == 0); }
            valid_motion = valid_motion && offsets_sorted[4] == 1 && offsets_sorted[5] == 2;
        }
        // If the rook is moving, flag the occlusion check then
        if (piece_id == 5 || piece_id == 11)
        {
            occlusionCheck = true;

            // Just check to see that the 5 shortest travel directions are zero.
            for (int i = 0; i < 5; i++) { valid_motion = valid_motion && (offsets_sorted[i] == 0); }
        }

        // Pawn logic. If we are not capturing, we only want the pawn to travel in a single direction once.
        if (!capturing)
        {
            // Black pawn logic
            if (piece_id == 6)
            {
                // Make sure shortest 5 directions are zero, then that the pawn moved the right way in at least one direction.
                for (int i = 0; i < 5; i++)  { valid_motion = valid_motion && (offsets_sorted[i] == 0); }
                valid_motion = valid_motion && (y == -1 || w == -1 || m_off == 1);
            }
            // White pawn logic.
            if (piece_id == 12)
            {
                // Make sure shortest 5 directions are zero, then that the pawn moved the right way in at least one direction.
                for (int i = 0; i < 5; i++) { valid_motion = valid_motion && (offsets_sorted[i] == 0); }
                valid_motion = valid_motion && (y == 1 || w == 1 || m_off == -1);
            }
        }
        // If we are capturing
        else
        {
            if (piece_id == 6)
            {
                // Ensure we are only moving along at most 2 directions
                for (int i = 0; i < 4; i++) { valid_motion = valid_motion && (offsets_sorted[i] == 0); }

                // The pawn diagonal is valid in a regular 4D game if y and w are progressing, m and t remain unchanged, and the magnitude of
                // some other direction of travel is one (which by process of elimination, must be x or z.)
                bool isValidFourDim = (y == -1 || w == -1) && (m_off == 0) && (t_off == 0) && offsets_sorted[4] == 1;

                // In a game of multiverse time travel chess, the pawn may also move towards the enemies parallel universe, and time travel once
                // In either direction. Note that since we make sure the other 4 motions are 0 in the for loop, this excludes any triagonals.
                bool isValidTimeTravel = (m_off == 1) && (t_off == 1 || t_off == -1);

                // A valid pawn motion is thus when either of these cases are true
                valid_motion = valid_motion && (isValidFourDim || isValidTimeTravel);
            }
            if (piece_id == 12)
            {
                for (int i = 0; i < 4; i++) {  valid_motion = valid_motion && (offsets_sorted[i] == 0); }

                // The pawn diagonal is valid in a regular 4D game if y and w are progressing, m and t remain unchanged, and the magnitude of
                // some other direction of travel is one (which by process of elimination, must be x or z.)
                bool isValidFourDim = (y == 1 || w == 1) && (m_off == 0) && (t_off == 0) && offsets_sorted[4] == 1;

                // In a game of multiverse time travel chess, the pawn may also move towards the enemies parallel universe, and time travel once
                // In either direction. Note that since we make sure the other 4 motions are 0 in the for loop, this excludes any triagonals.
                bool isValidTimeTravel = (m_off == -1) && (t_off == 1 || t_off == -1);

                // A valid pawn motion is thus when either of these cases are true
                valid_motion = valid_motion && (isValidFourDim || isValidTimeTravel);
            }
        }

        // Assume we pass the occlusion check, in case the piece triggering it only travels one square and fails to trigger the loop or check.
        bool pass_occlusion_check = true;
        
        // If our piece is one that must be occlusion checked
        if (occlusionCheck)
        {
            // Set the max steps target to be the magnitude of the largest direction traveld.
            int steps = offsets_sorted[5];

            // Go through the offsets array and set them to all have a magnitude of 1 or 0, respecting their original sign.
            for (int i = 0; i < 6; i++)
            {
                if (offsets[i] < 0) { offsets[i] = -1; }
                else if (offsets[i] > 0) { offsets[i] = 1; }
                else { offsets[i] = 0; }
            }

            // Calculate the offset converted to an index stepper. This *shouldnt* run into an out of bounds because we're assuming
            // we're moving from a valid square, to a valid square. Therefore, every square in between should also be valid
            int index_stepper = offsets[0] + offsets[1] * 4 + offsets[2] * 16 + offsets[3] * 64;

            // Starting one square ahead of our starting board (to avoid the piece thats actually moving, and stopping one short of the target
            // (to avoid triggering when capturing, as that logic and avoiding friendly fire is already implemented)
            for (int i = 1; i < steps; i++)
            {
                // Starting at the id_start subtract the index stepper (equivalent of traveling 1 move along the vector of travel) i times.
                // We are subtracting instead of adding because... for uh... Idk it makes it work.
                int target_id = id_start - index_stepper * i;

                // Get the board at the new t and m coordinate.
                BoardSerializer target_board = GetBoardSerializer(bfrom.tcoord + offsets[4] * i * 2, bfrom.mcoord + offsets[5] * i);
                
                // If we find a board, then test to see the target square on it is empty
                if (target_board != null) { pass_occlusion_check = pass_occlusion_check && (target_board.GetSquare(target_id) == 0); }
                // Other wise, just autofail the test. Cant travel on a board that doesnt exist. Might change this later.
                else { pass_occlusion_check = false; }
            }
        }

        // Apply the occlusion check to the move test
        valid_motion = valid_motion && pass_occlusion_check;

        // Return whether or not the piece is allowed to move that way.
        return valid_motion;
    }

    Vector3 CalculateTimelineOffset()
    {
        return (t_offset * rt * 0.40f + -z_offset * up) / this.transform.lossyScale.x;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///          Preview Board Control          ///
    ///                                         ///
    ///////////////////////////////////////////////

    // The next group of methods are responsible for placing boards on the game table, updating their pointers accordingly,
    // and gracefully returning the boards to the stack. PlacePreviewBoards() determines how many boards to place and where,
    // then actually places them and assigns pointers accordingly with some helper functions.

    void PlacePreviewBoards(BoardSerializer bfrom, BoardSerializer bto, int id_from, int id_to, int piece)
    {
        // Get the t and m coordinates from both our boards
        int t_from = bfrom.tcoord;
        int t_to = bto.tcoord;
        int m_from = bfrom.mcoord;
        int m_to = bto.mcoord;

        // Clear out our preview buffers
        preview_buffer0 = null;
        preview_buffer1 = null;

        // Reserve a spot for the board we are currently moving around
        BoardSerializer preview = null;

        // Declare if we are moving to a playable board
        bool bto_isPlayable = (bto.nt == null);

        // Advance time in the original board by one
        preview = PlacePreviewBoardAtCoord(t_from + 1, m_from, bfrom, 0);

        // Update the pointers from the old board to the new preview
        SetPointersNext(bfrom, preview);

        // Update the reference to the front root linked list if we advanced the main timeline.
        if (m_from == 0) { front_root = preview; }

        // Declare a vector that corresplonds to where we will place the timeline arrows
        Vector3 TimelineOffset = CalculateTimelineOffset();
        
        // Get the root board at multiverse line m, so that we can advance its arrow by one.
        BoardSerializer root_at_m = GetRootAtM(m_from);
        
        // Advance the root timelines time arrow indicator to the new board placed.
        if (root_at_m != null) { root_at_m.SetTimelineArrow(root_at_m.GetTimelineArrowRoot().transform.position, preview.transform.position + TimelineOffset); }

        // If moving to a non playable board (i.e. Muliverse)
        if (!bto_isPlayable)
        {
            int t_targ = bto.tcoord;
            bool isBlacksTurn = ((bto.tcoord & 1) == 1);

            int m_targ = 0;
            BoardSerializer multiverse_sentinal = root_serializer;
            BoardSerializer front_sentinal = front_root;


            if (isBlacksTurn)
            {
                // While we can still move root_down, move the multiverse sentinal to the lowest possible timeline. Same with the front
                while (multiverse_sentinal.root_dw != null) { multiverse_sentinal = multiverse_sentinal.root_dw; }
                while (front_sentinal.front_dw != null) { front_sentinal = front_sentinal.front_dw; }

                // Record the multiverse coordinate we reached, minus one to place a new board there
                m_targ = multiverse_sentinal.mcoord - 1;

                // Place our preview board there
                preview = PlacePreviewBoardAtCoord(t_to + 1, m_targ, bto, 1);
                
                // Update the pointers
                SetPointersRootDown(multiverse_sentinal, preview);
                SetPointersFrontDown(front_sentinal, preview);

                // And update the preview
                preview.SetTimelineArrow(bto.transform.position + TimelineOffset, preview.transform.position + TimelineOffset);
            }
            else
            {
                // While we can still move root_up, move the multiverse sentinal to the lowest possible timeline. Same with the front
                while (multiverse_sentinal.root_up != null) { multiverse_sentinal = multiverse_sentinal.root_up; }
                while (front_sentinal.front_up != null) { front_sentinal = front_sentinal.front_up; }

                // Record the multiverse cooridnate we reach, add one to place a new board as the furthest root_up
                m_targ = multiverse_sentinal.mcoord + 1;
                
                // Place our preview board there
                preview = PlacePreviewBoardAtCoord(t_to + 1, m_targ, bto, 1);

                SetPointersRootUp(multiverse_sentinal, preview);
                SetPointersFrontUp(front_sentinal, preview);

                // And update the preview
                preview.SetTimelineArrow(bto.transform.position + TimelineOffset, preview.transform.position + TimelineOffset);
            }
        }
        // If we are moving between two valid, advance time by one in the target timeline as well
        else
        {
            // Check to make sure we arent moving along a single board
            if (bto != bfrom)
            {
                // Place the preview in front of the destination board
                preview = PlacePreviewBoardAtCoord(t_to + 1, m_to, bto, 1);
                
                // Update the pointers
                SetPointersNext(bto, preview);
                
                // Get the root at the m coordinate desired
                root_at_m = GetRootAtM(m_to);

                // If we found a root serializer, advance the timeline arrow to the new preview board
                if (root_at_m != null) { root_at_m.SetTimelineArrow(root_at_m.GetTimelineArrowRoot().transform.position, preview.transform.position + TimelineOffset); }
                
                // And update the front root reference if we advanced the main timeline
                if (m_to == 0) { front_root = preview; }
            }
        }

    }

    // PlacePreviewBoardAtCoord places a preview board at the specified coordinate. Utilizes a lower level stack method,
    // PopOffPreviewStack() to actually get the preview board. It is also responsible for updated the boards game state
    // to copy the one that it spawns from, as each board must come from another one in the game table. Additionally,
    // A reference to the new board spawned is put in one of two buffer spots, to later be referenced as a global variable
    // so that pieces can be moved on them.

    BoardSerializer PlacePreviewBoardAtCoord(int t, int m, BoardSerializer b, int buffer_target)
    {
        // Get a fresh board off the storage stack
        BoardSerializer preview_board = PopOffPreviewStack();

        // Place the new board on top of our turn history stack
        preview_board.TurnStackNext = turn_stack_top;
        turn_stack_top = preview_board;
                    
        // If we are placing a board that is advancing time as a result of a piece being moved FROM it, write the to and from preview buffers
        // to be that board. Otherwise, just write the to-preview buffer to be the new preview board.
        if (buffer_target == 0)
        {
            preview_buffer0 = preview_board;
            preview_buffer1 = preview_board;
        }
        else { preview_buffer1 = preview_board; }

        // Set the m and t coordinates of the new board.
        preview_board.tcoord = t;
        preview_board.mcoord = m;

        // Move the preview board into place
        preview_board.gameObject.transform.position = root_serializer.gameObject.transform.position + m * m_offset * fw + t * t_offset * rt;

        // Send the board state from the source board to the preview board
        b.SerializeBoard();
        preview_board.ReceivePayload(b);

        // return a reference to the preview board so that PlacePreviewBoards() can access it to assign the rest of the pointers needed.
        return preview_board;

    }

    // While not used by any of the above functions, PlaceOnPreviewStack() will gracefully return a gameboard back to the stack they came from.
    void PlaceOnPreviewStack(BoardSerializer b)
    {
        // Reparent and move the board to the top of the stack
        b.transform.parent = stack_top.transform.parent;
        b.transform.position = stack_top.transform.position;
        b.transform.rotation = stack_top.transform.rotation;


        // If the board we are removing has a previous board
        if (b.pr != null)
        {
            b.pr.GetPieceArrow().SetActive(false);

            // Copy the front_up and front_down pointers to the previous board
            b.pr.front_up = b.front_up;
            b.pr.front_dw = b.front_dw;

            // Get the root board of the timeline
            BoardSerializer root_at_m = GetRootAtM(b.mcoord);

            // Return the timeline arrow from the root of the timeline to the preview boards position.
            Vector3 TimelineOffset = CalculateTimelineOffset();
            if (root_at_m != null) { root_at_m.SetTimelineArrow(root_at_m.GetTimelineArrowRoot().transform.position, b.pr.transform.position + TimelineOffset); }
        }

        // If we have front references, follow that and set its front down to the previous board (null if there is none)
        if (b.front_up != null) { b.front_up.front_dw = b.pr; }
        if (b.front_dw != null) { b.front_dw.front_up = b.pr; }

        // Clear the boards front references, just in case
        b.front_up = null;
        b.front_dw = null;

        // Change the GameStateControllers reference to the top of the turn history stack if we can.
        if (b.TurnStackNext != null) { turn_stack_top = b.TurnStackNext; }

        // Clear the returning boards turn stack reference, just in case.
        b.TurnStackNext = null;

        if (b.pr != null && b.mcoord == 0)
        {
            front_root = b.pr;
        }

        // If the board had a previous reference, clear the previous boards next reference (which was our board)
        if (b.pr != null) { b.pr.nt = null; }
        
        // Clear our boards previous reference
        b.pr = null;

        // See the above
        if (b.root_up != null) { b.root_up.root_dw = null; }
        b.root_up = null;

        // See the above
        if (b.root_dw != null) { b.root_dw.root_up = null; }
        b.root_dw = null;

        // Hide the boards Timeline arrow gizmo
        b.ClearTimelineArrow();

        // So, b.nt should be null, otherwise we probably shouldnt be returning it to the stack. So we can just write over this value.
        // Place b.nt on top of the preview stack.
        b.nt = stack_top;
        stack_top = b;

        b.gameObject.SetActive(false);

        // Increase the size of the stack to tell us we added a board to it.
        stack_size++;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///            Stack functions              ///
    ///                                         ///
    ///////////////////////////////////////////////

    BoardSerializer PopOffPreviewStack()
    {
        BoardSerializer top = stack_top;
        stack_top = top.nt;
        top.nt = null;
        stack_size--;
        if (stack_size < 4) { InstantiateToStack(); }
        top.gameObject.SetActive(true);
        return top;
    }

    void InstantiateToStack()
    {
        BoardSerializer b = (BoardSerializer)VRCInstantiate(stack_top.gameObject).GetComponent(typeof(UdonBehaviour));
        b.gameObject.name = "4D-chess-p" + instantiations;
        b.InitializeBoard();
        PlaceOnPreviewStack(b);
        b.transform.localScale = root_serializer.transform.localScale;
        
        instantiations++;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///          Place Preview Helpers          ///
    ///                                         ///
    ///////////////////////////////////////////////

    /// Helper functions for placing preview boards. Just assigns pointers accordingly.

    // Set the pointers when placing bser_target ahead of bser_source in the timeline.
    public void SetPointersNext(BoardSerializer bser_source, BoardSerializer bser_target)
    {
        // Set up the pointers accordingly.
        bser_source.nt = bser_target;
        bser_source.nt.pr = bser_source;

        bser_target.front_up = bser_source.front_up;
        bser_target.front_dw = bser_source.front_dw;

        if (bser_target.front_up != null) { bser_target.front_up.front_dw = bser_target; }
        if (bser_target.front_dw != null) { bser_target.front_dw.front_up = bser_target; }


        bser_source.front_up = null;
        bser_source.front_dw = null;
    }

    // Assign the pointers when adding bser_target up or down from bser_source
    public void SetPointersRootDown(BoardSerializer bser_source, BoardSerializer bser_target)
    {
        bser_source.root_dw = bser_target;
        bser_source.root_dw.root_up = bser_source;
    }
    public void SetPointersFrontDown(BoardSerializer bser_source, BoardSerializer bser_target)
    {
        bser_source.front_dw = bser_target;
        bser_source.front_dw.front_up = bser_source;
    }
    public void SetPointersRootUp(BoardSerializer bser_source, BoardSerializer bser_target)
    {
        bser_source.root_up = bser_target;
        bser_source.root_up.root_dw = bser_source;
    }
    public void SetPointersFrontUp(BoardSerializer bser_source, BoardSerializer bser_target)
    {
        bser_source.front_up = bser_target;
        bser_source.front_up.front_dw = bser_source;
    }


    ///////////////////////////////////////////////
    ///                                         ///
    ///          Game State Functions           ///
    ///                                         ///
    ///////////////////////////////////////////////

    public int DeterminePresent()
    {
        BoardSerializer sentinal_serializer0 = front_root;
        BoardSerializer sentinal_serializer1 = front_root;
        int present = front_root.tcoord;

        BoardSerializer target_board = front_root;
        while (sentinal_serializer0.front_up != null && sentinal_serializer1.front_dw != null)
        {
            sentinal_serializer0 = sentinal_serializer0.front_up;
            sentinal_serializer1 = sentinal_serializer1.front_dw;
            present = Mathf.Min(present, sentinal_serializer0.tcoord, sentinal_serializer1.tcoord);
        }
        if (sentinal_serializer0.front_up != null) { sentinal_serializer0 = sentinal_serializer0.front_up; }
        if (sentinal_serializer1.front_dw != null) { sentinal_serializer1 = sentinal_serializer1.front_dw; }
        present = Mathf.Min(present, sentinal_serializer0.tcoord, sentinal_serializer1.tcoord);
        
        present_root.transform.position = rootObject.transform.position + (t_offset * present + xy_offset * 1.5f) * rt;

        requiredMoveIsWhite = (present & 1) == 0;
        present_tcoord = present;
        return present;
    }
    // Go through the future most boards (the playable ones), and indicate which ones are within the active timelines,
    // and have tcoords = to the absolute present. These boards must be played before handing over to the next player.
    public void IndicateRequiredBoards()
    {
        BoardSerializer sentinal_serializer0 = front_root;
        BoardSerializer sentinal_serializer1 = front_root;

        ConditionalIndicator(sentinal_serializer0);

        BoardSerializer target_board = front_root;
        while (sentinal_serializer0.front_up != null && sentinal_serializer1.front_dw != null)
        {
            sentinal_serializer0 = sentinal_serializer0.front_up;
            sentinal_serializer1 = sentinal_serializer1.front_dw;
            ConditionalIndicator(sentinal_serializer0);
            ConditionalIndicator(sentinal_serializer1);
        }
        if (sentinal_serializer0.front_up != null) { sentinal_serializer0 = sentinal_serializer0.front_up; }
        if (sentinal_serializer1.front_dw != null) { sentinal_serializer1 = sentinal_serializer1.front_dw; }

        ConditionalIndicator(sentinal_serializer0);
        ConditionalIndicator(sentinal_serializer1);
    }

    // Helper function for IndicateRequiredBoards(). Tests if the tcoord matches the present, and clears out the previous ones
    // indicator just in case it was previously required.
    public void ConditionalIndicator(BoardSerializer bser)
    {
        if (bser.pr != null) { bser.pr.SetIndicator(false); }
        bser.SetIndicator(bser.tcoord == present_tcoord);
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///      Gizmo and Indicator Control        ///
    ///                                         ///
    ///////////////////////////////////////////////


    // Activate and set up the visualizer for the current piece that is trying to be moved.
    public void SetVisualizers(int piece_id, int xi, int yi, int zi, int wi, int ti, int mi)
    {
        SetBoardBounds(mi);
        SetMaterialProps(VisualizerMaterial, ti, mi);
        SetMaterialProps(VisualizerMaterial0, ti, mi);

        VisualizerRoot.gameObject.SetActive(true);
        VisualizerRoot.transform.position = PosFromCoord(xi, yi, zi, wi, ti, mi);
        VisualizerMaterial.SetVector("_Coordinate", new Vector4(xi, yi, zi, wi));
        VisualizerMaterial0.SetVector("_Coordinate", new Vector4(xi, yi, zi, wi));

        for (int i = 0; i < Visualizers.Length; i++)
        {
            Visualizers[i].SetActive(false);
        }

        // If the king is moving
        if (piece_id == 1 || piece_id == 7) { Visualizers[0].SetActive(true); }
        // If the queen is moving
        if (piece_id == 2 || piece_id == 8) { Visualizers[1].SetActive(true); }
        // If the bishop is moving
        if (piece_id == 3 || piece_id == 9) { Visualizers[2].SetActive(true); }
        // If the knight is moving
        if (piece_id == 4 || piece_id == 10) { Visualizers[3].SetActive(true); }
        // If the rook is moving
        if (piece_id == 5 || piece_id == 11) { Visualizers[4].SetActive(true); }
        if (piece_id == 6)
        {
            Visualizers[5].SetActive(true);
            Visualizers[6].SetActive(true);

        }
        if (piece_id == 12)
        {
            Visualizers[7].SetActive(true);
            Visualizers[8].SetActive(true);
        }

    }

    public void SetBoardBounds(int m)
    {
        root_coords = new int[7];
        front_coords = new int[7];

        BoardSerializer root_start = GetRootAtM(m);
        BoardSerializer front_start = GetFrontAtM(m);

        if (root_start != null && front_start != null)
        {
            BoardSerializer root_sentinal = root_start;
            BoardSerializer front_sentinal = front_start;

            root_coords[3] = root_sentinal.tcoord;
            front_coords[3] = front_sentinal.tcoord;

            for (int i = 1; i <= 3; i++)
            {
                if (root_sentinal.root_dw != null)
                {
                    root_sentinal = root_sentinal.root_dw;
                    root_coords[3 - i] = root_sentinal.tcoord;
                }
                if (front_sentinal.front_dw != null)
                {
                    front_sentinal = front_sentinal.front_dw;
                    front_coords[3 - i] = front_sentinal.tcoord;
                }
            }

            root_sentinal = root_start;
            front_sentinal = front_start;
            for (int i = 1; i <= 3; i++)
            {
                if (root_sentinal.root_up != null)
                {
                    root_sentinal = root_sentinal.root_up;
                    root_coords[3 + i] = root_sentinal.tcoord;
                }
                if (front_sentinal.front_up != null)
                {
                    front_sentinal = front_sentinal.front_up;
                    front_coords[3 + i] = front_sentinal.tcoord;
                }
            }
        }

    }

    public void SetMaterialProps(Material mat, int t, int m)
    {
        mat.SetInt("_Min0", root_coords[0]);
        mat.SetInt("_Min1", root_coords[1]);
        mat.SetInt("_Min2", root_coords[2]);
        mat.SetInt("_Min3", root_coords[3]);
        mat.SetInt("_Min4", root_coords[4]);
        mat.SetInt("_Min5", root_coords[5]);
        mat.SetInt("_Min6", root_coords[6]);

        mat.SetInt("_Max0", front_coords[0]);
        mat.SetInt("_Max1", front_coords[1]);
        mat.SetInt("_Max2", front_coords[2]);
        mat.SetInt("_Max3", front_coords[3]);
        mat.SetInt("_Max4", front_coords[4]);
        mat.SetInt("_Max5", front_coords[5]);
        mat.SetInt("_Max6", front_coords[6]);

        mat.SetInt("_StartTime", t);
        mat.SetInt("_StartMult", m);
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///         Linked List functions           ///
    ///                                         ///
    ///////////////////////////////////////////////
 
    // Functions for getting boards through the linked list system.

    // Get the board at the specified t and m coordinates.
    BoardSerializer GetBoardSerializer(int t, int m)
    {
        BoardSerializer sentinal_serializer = root_serializer;
        if (m > 0)
        {
            for (int i = 0; i < m; i++)
            {
                if (sentinal_serializer != null) { sentinal_serializer = sentinal_serializer.root_up; }
            }
        }
        else if (m < 0)
        {
            for (int i = 0; i < -m; i++)
            {
                if (sentinal_serializer != null) { sentinal_serializer = sentinal_serializer.root_dw; }
            }
        }
        while (sentinal_serializer != null && sentinal_serializer.tcoord < t) { sentinal_serializer = sentinal_serializer.nt; }
        if (sentinal_serializer != null && sentinal_serializer.tcoord > t) { sentinal_serializer = null; }
        return sentinal_serializer;
    }

    // Get the root of the timeline at the specified m coordinate.
    BoardSerializer GetRootAtM(int m)
    {
        BoardSerializer sentinal_serializer0 = root_serializer;
        if (m < 0)
        {
            while (sentinal_serializer0 != null && sentinal_serializer0.mcoord != m) { sentinal_serializer0 = sentinal_serializer0.root_dw; }
        }
        else if (m > 0)
        {
            while (sentinal_serializer0 != null && sentinal_serializer0.mcoord != m) { sentinal_serializer0 = sentinal_serializer0.root_up; }
        }
        return sentinal_serializer0;
    }
    BoardSerializer GetFrontAtM(int m)
    {
        BoardSerializer sentinal_serializer0 = front_root;
        if (m < 0)
        {
            while (sentinal_serializer0 != null && sentinal_serializer0.mcoord != m) { sentinal_serializer0 = sentinal_serializer0.front_dw; }
        }
        else if (m > 0)
        {
            while (sentinal_serializer0 != null && sentinal_serializer0.mcoord != m) { sentinal_serializer0 = sentinal_serializer0.front_up; }
        }
        return sentinal_serializer0;
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///      Low level Snapping functions       ///
    ///                                         ///
    ///////////////////////////////////////////////

    public void UpdateBasis()
    {
        // so long as we have a root object to reference
        if (rootObject != null)
        {
            // Get the objects transform
            Transform t = rootObject.transform;

            // Set our origin for the coordinate systems
            root = t.position;

            // Update our basis to its local basis
            rt = t.right;
            up = t.up;
            fw = t.forward;

            // Scale our offsets by its global scale
            xy_offset = 0.0825f * t.lossyScale.z;
            z_offset = 0.1235f * t.lossyScale.z;
            z_scaling = 0.9f;
            w_offset = 0.4125f * t.lossyScale.z;
            t_offset = 1.0f * t.lossyScale.z;
            m_offset = 2.5f * t.lossyScale.z;

        }
    }

    private Vector3 PosFromCoord(int xi, int yi, int zi, int wi, int ti, int mi)
    {
        // Okay, recall our chess set has 4 coordinates. The xy by chess in the chess convention is scaled the same, 
        // and represents our traditional chess board. The z direction is up and down, and the board scales by some
        // factor each time it goes up. The w direction is "Hyper-forward", and is a fixed offset.
        // To get from the bottom corner to any other given one given a coordinate, we need to that into account.

        // Easy, z coordinate multiplied the scale of the offset, times the up vector.
        Vector3 z = zi * z_offset * up;

        // Since the z coordinate scaleds by a factor as we go up, we want to move this offset to the center before scaling
        Vector3 center = xy_offset * 1.5f * (rt + fw);

        // Determine the scaling factor. Scales by a fixed factor each time up, so we'll use a for loop. On the zeroth layer,
        // coordinate.z = 0, the loop doesnt run. On the first, coordinate.z = 1 so it runs once. Start at one
        float scaler = 1.0f;

        // Each time we go up, multiply by our scaling factor.
        for (int i = 0; i < zi; i++)
        {
            scaler *= z_scaling;
        }

        // Next we want to undo our center vector, but multiplied by the scaling we specified.
        Vector3 un_center = -center * scaler;

        // XY offset is pretty easy. Just make sure to account for the scaling.
        Vector3 xy = (xi * rt + yi * fw) * xy_offset * scaler;

        // W offset is likewise easy, just apply a fixed offset
        Vector3 w = fw * wi * w_offset;

        Vector3 position_from_coordinate = root + center + z + un_center + xy + w;

        // Now lets go through the whole process. Move to center, move up, uncenter, and apply the rest of the coordinates
        return position_from_coordinate + rt * ti * t_offset + fw * mi * m_offset;

        // There we go. Now once the values are fine tuned, all the hypersquares should line themselves up based on their coordinates.
    }

    ///////////////////////////////////////////////
    ///                                         ///
    ///         Small Helper Functions          ///
    ///                                         ///
    ///////////////////////////////////////////////

    // Determine the color of a piece based on its piece id.
    bool isBlackPiece(int piece_id)
    {
        return (piece_id < 7);
    }

    // Interleave multiverse coordinates (-inf to +inf) to a positive index arranged (0, 1, -1, 2, -2...) -> (0, 1, 2, 3, 4...)
    int m_to_i(int m)
    {
        int s = 0;
        if (m < 0) { s = 1; }
        return 2 * Mathf.Abs(m) - s;
    }

    // Undo the above interleaving to get the multiverse coordinate in an index.
    int i_to_m(int i)
    {
        return ((i + 1) / 2) * (1 - 2 * (i & 1));
    }

    // Convert a 4-Vector to an index ranging 0-255
    public int CoordToIndex(int xi, int yi, int zi, int wi)
    {
        return xi + yi * 4 + zi * 16 + wi * 64;
    }

    public Vector3 PosFromIndex(int i, int ti, int mi)
    {
        int x = (i >> 0) & 3;
        int y = (i >> 2) & 3;
        int z = (i >> 4) & 3;
        int w = (i >> 6) & 3;

        return PosFromCoord(x, y, z, w, ti, mi);
    }

    // public getter for determining what turn we're on.
    public int GetTurns()
    {
        return turns;
    }

}




