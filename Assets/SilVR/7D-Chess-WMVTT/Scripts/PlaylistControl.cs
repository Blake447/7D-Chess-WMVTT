﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using VRC.SDK3;
using VRC.SDK3.Video.Components.AVPro;
using VRC.SDK3.Video.Components;
using VRC.SDK3.Video.Components.AVPro;
using VRC.SDK3.Video.Components.Base;



public class PlaylistControl : UdonSharpBehaviour
{
    public VRCUrl url0 = new VRCUrl("https://www.youtube.com/watch?v=h_CYgJKxkFM");
    public VRCUrl url1 = new VRCUrl("https://www.youtube.com/watch?v=Vk6tLRodXxA");
    public VRCUrl url2 = new VRCUrl("https://www.youtube.com/watch?v=XvH20cbuLK0");




    public VRCAVProVideoPlayer video_player;


    public void PlayVideo()
    {
        video_player.Play();
    }
    public void StopVideo()
    {
        video_player.Stop();
    }
    public void PlayGettingStarted()
    {
        video_player.PlayURL(url0);
    }
    public void PlayConstructingBoard()
    {
        video_player.PlayURL(url1);
    }
    public void PlayMovingInFourD()
    {
        video_player.PlayURL(url2);
    }

    public override void Interact()
    {
        PlayGettingStarted();
    }

    void Start()
    {
        
    }
}
