﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class BoardSerializer : UdonSharpBehaviour
{
    public GameStateController gsc;
    public GameObject ReferencePieces;


    public int[] squares = new int[256];

    GameObject piece_arrow;
    GameObject Indicator;
    GameObject TimelineArrowRoot;
    GameObject TimelineArrow;
    GameObject piecesRoot;


    //public MeshFilter[] referenceMeshesLOD;

    public MeshFilter[] referenceMeshes;
    public MeshRenderer[] referenceMaterials;


    public string serial1;
    public string serial2;
    public string serial3;
    public string serial4;

    public BoardSerializer TurnStackNext;

    public int turn_number;

    public BoardSerializer nt;
    public BoardSerializer pr;

    public BoardSerializer root_up;
    public BoardSerializer root_dw;

    public BoardSerializer front_up;
    public BoardSerializer front_dw;
    public int tcoord = 0;
    public int mcoord = 0;


    int[] start_state = new int[256] {  
                                        5, 4, 4, 5,  6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,
                                        3, 1, 2, 3,  6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,
                                        3, 2, 6, 3,  6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,
                                        5, 4, 4, 5,  6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,
                                        6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
                                        6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
                                        6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
                                        6, 6, 6, 6,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
                                        0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,
                                        0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,
                                        0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,
                                        0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,
                                        0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12, 11,10,10,11,
                                        0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,  9, 7, 8, 9,
                                        0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12,  9, 8,12, 9,
                                        0, 0, 0, 0,  0, 0, 0, 0, 12,12,12,12, 11,10,10,11
                                      };

    public int GetSquare(int i)
    {
        return squares[i];
    }
    public GameObject GetPieceArrow()
    {
        return piece_arrow;
    }
    public GameObject GetTimelineArrowRoot()
    {
        return TimelineArrowRoot;
    }

    public void InitializeBoard()
    {
        referenceMeshes = new MeshFilter[6];
        referenceMaterials = new MeshRenderer[2];

        for (int i = 0; i < 6; i++)
        {
            referenceMeshes[i] = ReferencePieces.transform.GetChild(i).GetComponent<MeshFilter>();
        }
        referenceMaterials[0] = ReferencePieces.transform.GetChild(6).GetComponent<MeshRenderer>();
        referenceMaterials[1] = ReferencePieces.transform.GetChild(7).GetComponent<MeshRenderer>();

        piecesRoot = this.transform.GetChild(0).gameObject;
        Transform Gizmos = this.transform.GetChild(1);
        piece_arrow = Gizmos.GetChild(0).gameObject;
        Indicator = Gizmos.GetChild(1).gameObject;
        TimelineArrowRoot = Gizmos.GetChild(2).gameObject;
        TimelineArrow = TimelineArrowRoot.transform.GetChild(0).gameObject;
        squares = start_state;

        //for (int i = 0; i < 256; i++)
        //{
        //    GameObject p = piecesRoot.transform.GetChild(i).gameObject;
        //}
        //UpdatePieces();

    }

    public void SetIndicator(bool value)
    {
        Indicator.SetActive(value);
    }

    public void ClearTimelineArrow()
    {
        GameObject arrow_root = TimelineArrowRoot;
        arrow_root.SetActive(false);
    }
    public void SetTimelineArrow(Vector3 from, Vector3 target)
    {
        float main_scale = gsc.transform.lossyScale.x;

        GameObject arrow_root = TimelineArrowRoot;
        GameObject arrow = TimelineArrow;

        arrow_root.SetActive(true);
        arrow_root.transform.position = from;

        float lateral_distance = 0.75f;
        float forward_distance = 0.50f;
        float segment_distance = 0.25f;
        float epsilon = .01f;

        float scaling = arrow_root.transform.localScale.x;

        Vector3 offset = (target - arrow.transform.position) / scaling;
        float lateral_projection = Vector3.Dot(offset, arrow_root.transform.right) / main_scale;
        float forward_projection = Vector3.Dot(offset, arrow_root.transform.forward) / main_scale;

        forward_projection = Mathf.Clamp(forward_projection, forward_distance, 10000000.0f);
        if (lateral_projection < 0.0f)
        {
            lateral_projection = Mathf.Clamp(lateral_projection, -10000000.0f, -lateral_distance);
            arrow.transform.rotation = arrow_root.transform.rotation * Quaternion.Euler(0, 0, 0);
        }
        else
        {
            lateral_projection = Mathf.Clamp(lateral_projection, lateral_distance, 10000000.0f);
            arrow.transform.rotation = arrow_root.transform.rotation * Quaternion.Euler(0, 0, 180);
        }

        float midsegment_scale = Mathf.Abs(lateral_projection) - segment_distance * 2.0f;
        float forsegment_scale = Mathf.Clamp(forward_projection - forward_distance, epsilon, 10000000.0f);

        GameObject arrowSeg0 = arrow.transform.GetChild(0).gameObject;
        GameObject arrowSeg1 = arrow.transform.GetChild(1).gameObject;
        GameObject arrowSeg2 = arrow.transform.GetChild(2).gameObject;
        GameObject arrowSeg3 = arrow.transform.GetChild(3).gameObject;
        GameObject arrowSeg4 = arrow.transform.GetChild(4).gameObject;

        arrowSeg1.transform.localScale = new Vector3(segment_distance, segment_distance, midsegment_scale);
        arrowSeg3.transform.localScale = new Vector3(segment_distance, segment_distance, forsegment_scale);

        arrowSeg2.transform.localPosition = new Vector3(-segment_distance * 1.0f - midsegment_scale, 0.0f, segment_distance * 1.0f);
        arrowSeg3.transform.localPosition = new Vector3(-segment_distance * 2.0f - midsegment_scale, 0.0f, segment_distance * 2.0f);
        arrowSeg4.transform.localPosition = new Vector3(-segment_distance * 2.0f - midsegment_scale, 0.0f, segment_distance * 2.0f + forsegment_scale - epsilon);
    }

    public void ResetArrow(GameObject arrow)
    {
        arrow.SetActive(false);
    }

    public void PointArrow(GameObject arrow, GameObject target)
    {
        float main_scale = gsc.gameObject.transform.lossyScale.x;

        arrow.SetActive(true);

        Vector3 offset = (target.transform.position - arrow.transform.position);
        float distance = offset.magnitude / main_scale;

        Quaternion rotation = Quaternion.LookRotation(offset.normalized, Vector3.up);

        arrow.transform.rotation = rotation;
        GameObject arrow_stem = arrow.transform.GetChild(0).gameObject;
        GameObject arrow_tip = arrow.transform.GetChild(1).gameObject;

        if (arrow_stem != null)
        {
            arrow_stem.transform.localScale = new Vector3(25, 25* (distance * 4.0f), 25);
        }
        if (arrow_tip != null)
        {
            arrow_tip.transform.localPosition = Vector3.forward * distance;
        }
    }

    public void PointArrowVector(GameObject arrow, Vector3 from, Vector3 to, float decrement)
    {
        float main_scale = gsc.gameObject.transform.lossyScale.x;


        arrow.SetActive(true);
        arrow.transform.position = from;

        Vector3 offset = (to - arrow.transform.position);
        float distance = offset.magnitude / (arrow.transform.localScale.x * main_scale) - decrement / main_scale;

        Quaternion rotation = Quaternion.LookRotation(offset.normalized, Vector3.up);

        arrow.transform.rotation = rotation;
        GameObject arrow_stem = arrow.transform.GetChild(0).gameObject;
        GameObject arrow_tip = arrow.transform.GetChild(1).gameObject;

        if (arrow_stem != null)
        {
            arrow_stem.transform.localScale = new Vector3(25, 25 * (distance * 4.0f), 25);
        }
        if (arrow_tip != null)
        {
            arrow_tip.transform.localPosition = Vector3.forward * distance;
        }
    }


    char CombineInts(int x, int y, int z, int w)
    {
        return (char)( (x & 15) + ((y & 15) << 4) + ((z & 15) << 8) + ((w & 15) << 12) );
    }
    int SplitInt(char ch, int i)
    {
        int j = i & 7;
        return ((int)ch >> (4 * j)) & 15;
    }
    public void SerializeBoard()
    {
        serial1 = "";
        serial2 = "";
        serial3 = "";
        serial4 = "";

        for (int j = 0; j < 16; j++)
        {
            serial1 += CombineInts(squares[0 + 4 * j + 0], squares[0 + 4 * j + 1], squares[0 + 4 * j + 2], squares[0 + 4 * j + 3]);
            serial2 += CombineInts(squares[64 + 4 * j + 0], squares[64 + 4 * j + 1], squares[64 + 4 * j + 2], squares[64 + 4 * j + 3]);
            serial3 += CombineInts(squares[128 + 4 * j + 0], squares[128 + 4 * j + 1], squares[128 + 4 * j + 2], squares[128 + 4 * j + 3]);
            serial4 += CombineInts(squares[192 + 4 * j + 0], squares[192 + 4 * j + 1], squares[192 + 4 * j + 2], squares[192 + 4 * j + 3]);
        }
    }
    void DeserializeBoard()
    {
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                squares[0   + 4 * i + j] = SplitInt(serial1[i], j);
                squares[64  + 4 * i + j] = SplitInt(serial2[i], j);
                squares[128 + 4 * i + j] = SplitInt(serial3[i], j);
                squares[192 + 4 * i + j] = SplitInt(serial4[i], j);
            }
        }
        UpdatePieces();
    }

    public void ReceivePayload(BoardSerializer b)
    {
        serial1 = b.serial1;
        serial2 = b.serial2;
        serial3 = b.serial3;
        serial4 = b.serial4;

        DeserializeBoard();
    }


    void UpdatePieces()
    {
        for (int i = 0; i < 256; i++)
        {
            GameObject p = piecesRoot.transform.GetChild(i).gameObject;
            int id = squares[i];
            if (id == 0) { p.gameObject.SetActive(false); }
            else
            {
                p.gameObject.SetActive(true);

                p.transform.GetComponent<MeshFilter>().sharedMesh = referenceMeshes[((id - 1) % 6)].sharedMesh;
                p.transform.GetComponent<MeshRenderer>().sharedMaterial = referenceMaterials[((id - 1) / 6) & 1].sharedMaterial;
                int isWhite = ((id - 1) / 6) & 1;

                p.transform.GetComponent<MeshRenderer>().SetPropertyBlock(gsc.ReturnPropertyBlock(isWhite));


                //p.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = referenceMeshesLOD[((id - 1) % 6) * 3 + 0].sharedMesh;
                //p.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh = referenceMeshesLOD[((id - 1) % 6) * 3 + 1].sharedMesh;
                //p.transform.GetChild(2).GetComponent<MeshFilter>().sharedMesh = referenceMeshesLOD[((id - 1) % 6) * 3 + 2].sharedMesh;

                //p.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = referenceMaterials[((id - 1) / 6) & 1].sharedMaterial;
                //p.transform.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial = referenceMaterials[((id - 1) / 6) & 1].sharedMaterial;
                //p.transform.GetChild(2).GetComponent<MeshRenderer>().sharedMaterial = referenceMaterials[((id - 1) / 6) & 1].sharedMaterial;


            }
        }
    }

    public void AddPiece(int x, int y, int z, int w, int pieceID)
    {
        squares[x + y * 4 + z * 16 + w * 64] = pieceID;
        UpdatePieces();
        SerializeBoard();
    }

    public void AddPieceID(int id, int pieceID)
    {
        squares[id] = pieceID;
        UpdatePieces();
        SerializeBoard();
    }

    public override void Interact()
    {
        UpdatePieces();
        SerializeBoard();
    }

    private void Update()
    {
    }
    void Start()
    {

    }
}
