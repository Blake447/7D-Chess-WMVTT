﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUpLODs : MonoBehaviour
{
    public GameObject SpacesRoots;
    public GameObject[] referenceSets;
    public GameObject[] LOD_set_ref;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 256; i++)
        {
            GameObject g = SpacesRoots.transform.GetChild(i).gameObject;
            
            GameObject g0 = Instantiate(LOD_set_ref[0]);
            GameObject g1 = Instantiate(LOD_set_ref[1]);
            GameObject g2 = Instantiate(LOD_set_ref[2]);

            g0.name = g.name + "_LOD0";
            g1.name = g.name + "_LOD1";
            g2.name = g.name + "_LOD2";

            g0.transform.parent = g.transform;
            g1.transform.parent = g.transform;
            g2.transform.parent = g.transform;


            g0.transform.localPosition = Vector3.zero;
            g0.transform.rotation = Quaternion.identity;

            g1.transform.localPosition = Vector3.zero;
            g1.transform.localRotation = Quaternion.identity;
            
            g2.transform.localPosition = Vector3.zero;
            g2.transform.localRotation = Quaternion.identity;



            LOD[] lods = new LOD[3];

            lods[0] = new LOD(0.20f, g0.GetComponents<MeshRenderer>());
            lods[1] = new LOD(0.05f, g1.GetComponents<MeshRenderer>());
            lods[2] = new LOD(0.005f, g2.GetComponents<MeshRenderer>());

            g.GetComponent<LODGroup>().SetLODs(lods);



        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
